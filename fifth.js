console.log(typeof(1), ": Type of the number 1")
console.log(typeof(1.5), ": Type of the number 1.5")
console.log(typeof(true), ": Type of \"true\"")
console.log(typeof('hello'), ": Type of \"hello\"")
console.log(typeof({}), ": Type of \"{}\"")
console.log(typeof(NaN), ": Type of \"NaN\"")
console.log(typeof(null), ": Type of \"null\"")
console.log(typeof(undefined), ": Type of \"undefined\"")
console.log(typeof(function(){}), ": Type of \"function\"")

let a = 9
let b = "10.3"
console.log(typeof(a), ": type conversion to int number : " , typeof(a.toString()))
console.log(typeof(b), ": type conversion to float number : " , typeof(parseFloat(b)),parseFloat(b))
console.log(typeof(b), ": type conversion to int number : " , typeof(parseInt(b)),parseInt(b))

for (var i =0 ; i <12 ; i++){
    if (i == 8) break;
    else console.log(i)     
}

 let result = (a > 10) ? "greater than 10" : "smaller than 10"
 console.log(result)

let app = (function()
{
    let carId = 1012
    console.log(carId)
    return;
})();  

let o = {
    carId:123,
    getId: function(){return this.carId}
}

console.log(o.getId())
